package com.deloitte.nycschool.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.deloitte.nycschool.network.onError
import com.deloitte.nycschool.network.onSuccess
import com.deloitte.nycschool.repository.HomeRepository
import kotlinx.coroutines.launch

/**
 * ViewModel for handling business logic, api calling of School List
 *
 * @property homeRepository
 */
class SchoolHomeViewModel(private val homeRepository: HomeRepository) : ViewModel() {

    var homeUiState: MutableLiveData<SchoolHomeUiState> = MutableLiveData()
        private set

    init {
        getSchoolListData()
    }

    fun getSchoolListData() = viewModelScope.launch {
        homeUiState.postValue(SchoolHomeUiState.Loading)

        homeRepository.fetchNycSchools().onSuccess {
            homeUiState.postValue(
                SchoolHomeUiState.Success(it)
            )
        }.onError {
            homeUiState.postValue(SchoolHomeUiState.Error)
            Log.e("response error", "${it.responseBodyData}")
        }
    }
}