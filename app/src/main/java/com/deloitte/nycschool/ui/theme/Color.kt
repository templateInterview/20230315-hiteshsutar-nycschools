package com.deloitte.nycschool.ui.theme

import androidx.compose.ui.graphics.Color

val LightBlue = Color(0xFFa4c8ff)

val md_theme_light_primary = Color(0xFF205fa6)
val md_theme_light_onPrimary = Color(0xFFffffff)
val md_theme_light_secondary = Color(0xFF555f71)

val md_theme_dark_primary = Color(0xFFa4c8ff)
val md_theme_dark_onPrimary = Color(0xFF003061)
val md_theme_dark_secondary = Color(0xFFbdc7dc)