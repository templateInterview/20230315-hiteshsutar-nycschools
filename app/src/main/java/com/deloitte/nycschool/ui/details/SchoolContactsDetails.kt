package com.deloitte.nycschool.ui.details

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.deloitte.nycschool.R
import com.deloitte.nycschool.model.SchoolItem
import com.deloitte.nycschool.ui.theme.Typography

@Composable
fun SchoolContactsDetails(schoolBasicData: SchoolItem) {
    Column(modifier = Modifier.padding(16.dp)) {
        Text(
            text = stringResource(R.string.contact),
            style = Typography.h6,
            modifier = Modifier.padding(bottom = 8.dp)
        )
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.Top
        ) {
            Column {
                val context = LocalContext.current

                Text(
                    text = "${schoolBasicData.primary_address_line_1}, ${schoolBasicData.city}, ${schoolBasicData.state_code}-${schoolBasicData.zip}".trimEnd(),
                    style = Typography.body1,
                    modifier = Modifier.padding(bottom = 8.dp)
                )

                val websiteUrl = schoolBasicData.website.toString()
                val url = if (!websiteUrl.startsWith("http")) {
                    "http://$websiteUrl"
                } else {
                    websiteUrl
                }

                Text(text = websiteUrl, style = Typography.subtitle1.copy(
                    color = Color.Blue
                ), modifier = Modifier
                    .clickable {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                        context.startActivity(intent)
                    }
                    .padding(bottom = 8.dp)
                )

                Text(
                    text = schoolBasicData.phone_number.toString(),
                    style = MaterialTheme.typography.body1,
                    modifier = Modifier.padding(bottom = 8.dp)
                )

            }
            if (schoolBasicData.latitude?.isNotEmpty() == true && schoolBasicData.longitude?.isNotEmpty() == true) {
                NavigationMapCta(
                    schoolBasicData.latitude.toDouble(), schoolBasicData.latitude.toDouble()
                )
            }
        }
    }

}

@Composable
fun NavigationMapCta(latitude: Double, longitude: Double) {
    val context = LocalContext.current
    val uri = Uri.parse("geo:$latitude,$longitude")
    val intent = Intent(Intent.ACTION_VIEW, uri)
    Image(painter = painterResource(id = R.drawable.ic_navigation),
        contentDescription = stringResource(
            R.string.find_in_map
        ),
        modifier = Modifier
            .size(35.dp)
            .padding(start = 10.dp)
            .clickable {
                context.startActivity(intent)
            })
}