package com.deloitte.nycschool.di

import com.deloitte.nycschool.repository.HomeRepository
import org.koin.dsl.module

/**
 * DI - All Repository will be added in this Module
 */
val repositoryModule = module {
    single { HomeRepository(get()) }
}
